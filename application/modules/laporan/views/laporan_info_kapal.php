<!-- filter propinsi -->
<?php
	$this->load->helper('html');
	echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
	// print_r(Modules::run('laporan/mst_filter/list_propinsi_array'));
	$attr_provinsi = array( 'name' => 'kapal',
                                  'label' => 'Kapal',
                                  'opsi' => Modules::run('laporan/mst_filter/list_kapal_array'),
                                  'value' => $id_kapal
                    );
          echo $this->mkform->input_select2($attr_provinsi);
?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Search</button>
              <!-- <button type="submit" class="btn btn-primary" name="export">Export</button> -->
            </div>
          </div>
  </div>
</div>


<html>

	<head>
		<style>				
			#content 	{
							width:1280px;
							max-height:800px;
							margin: auto;

						}
			#header		{
							border: 1px solid;
							padding: 10px;
							/*height:10%;*/
						}		
						
			#menu		{
							float:right;
							margin:10px;
							width:50%;
						}
						
			
			.navigasi	{
							border: 1px solid;
							padding: 10px;
							margin-top: 10px;
							/*height:20%;*/
						}
						
						
			#isi		{
							width:45%;
							height:100%;
							margin:10px;
							float:left;
						}
						
			.kiri		{
							border: 1px solid;
							margin-top: 10px;
							padding: 10px;
							/*height:40%;*/
						}
						
			.center		{
							text-align:center;
						}
		
			.garis		{
							border-bottom-style:solid;
							border-width:1px;
						}
			table{
							width: 100%;
						}
			.td-center 			{
							padding:10px;
							text-align:center;
						}
			img			{
						    display: block;
						    margin-left: auto;
						    margin-right: auto;
						}

		</style>
	<head>
	<body>
		<div style="width:100%;border:1px solid #ccc;overflow:auto;">
		<div id="content">
			<div id="header">
				<h2 class="center">Evaluasi & Profil Kapal Inka Mina <br/>INKA MINA</h2>
			</div>
			
			<div id="menu">
				<div class="navigasi">
					<div id="kanan">  
					  <p style="font-weight:bold;">Permasalahan pokok :</p>
					  <p class="garis"> </p>
				</div>
				</div>
				<div class="navigasi">
					<p style="font-weight:bold;" class="center">Foto-Foto Kegiatan </p>
					<table class='tbl_gambar'>
						<tr>
							<td class='td-center'>
								<?php 
									$file = (isset($detail_kapal[0]->foto_pembuatan))? $detail_kapal[0]->foto_pembuatan: '0.jpg';
									$image_properties = array(
									          'src' => 'uploads/'.$file,
									          'class' => 'post_images',
									          'width' => '100',
									          'height' => '100'
									);
									echo img($image_properties);

								?>
							</td>
							<td class='td-center'>
								<?php 
									$file = (isset($detail_kapal[0]->foto_sea_trial))? $detail_kapal[0]->foto_sea_trial: '0.jpg';
									$image_properties['src'] = 'uploads/'.$file;
									echo img($image_properties);

								?>
							</td>
							<td class='td-center'>
								<?php 
									$file = (isset($detail_kapal[0]->foto_oprasional))? $detail_kapal[0]->foto_oprasional: '0.jpg';
									$image_properties['src'] = 'uploads/'.$file;
									echo img($image_properties);

								?>
							</td>
						<tr>
						<tr>
							<td class='td-center'>proses pembuatan</td>
							<td class='td-center'>proses sea trial</td>
							<td class='td-center'>operasional</td>
						<tr>
					</table>
				</div>
				<div class="navigasi">
					<table class='tbl_gambar'>
						<tr>
							<td colspan="2" class='td-center' style="font-weight:bold;">Surat Ijin Usaha Penangkapan</td>
							<td colspan="2" class='td-center' style="font-weight:bold;">Bas ke KuB</td>
							
						<tr>
						<tr>
							<td class='td-center'>
								<?php 
									$file = (isset($detail_kapal[0]->foto_siup1))? $detail_kapal[0]->foto_siup1: '0.jpg';
									$image_properties['src'] = 'uploads/'.$file;
									$image_properties['height'] = "150";
									echo img($image_properties);

								?>
							</td>
							<td class='td-center'>
								<?php 
									$file = (isset($detail_kapal[0]->foto_siup2))? $detail_kapal[0]->foto_siup2: '0.jpg';
									$image_properties['src'] = 'uploads/'.$file;
									echo img($image_properties);

								?>
							</td>
							<td class='td-center'>
								<?php 
									$file = (isset($detail_kapal[0]->foto_bast1))? $detail_kapal[0]->foto_bast1: '0.jpg';
									$image_properties['src'] = 'uploads/'.$file;
									echo img($image_properties);

								?>
							</td>
							<td class='td-center'>
								<?php 
									$file = (isset($detail_kapal[0]->foto_bast2))? $detail_kapal[0]->foto_bast2: '0.jpg';
									$image_properties['src'] = 'uploads/'.$file;
									echo img($image_properties);

								?>
							</td>
						<tr>
					</table>
				</div>
			</div>
			
			<div id="isi">
				<div class="kiri">
				<p class="garis" style="font-weight:bold;">Profil dasar</p>
				<table>
						<tr>
							<td  class="garis" style="width:150px">Nama Kapal </td>
							<td class="garis">:</td>
							<td   class="garis"><?php echo (isset($detail_kapal[0]->nama_kapal))? $detail_kapal[0]->nama_kapal: ' ';?></td>
						</tr>
							
						<tr>
							<td class="garis">Sumber Anggaran </td>		
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->sumber_anggaran))? $detail_kapal[0]->sumber_anggaran:' ';?></td>
						</tr >
							
						<tr>
							<td class="garis">Ukuran</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->gt))? $detail_kapal[0]->gt:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Jenis Alat Tangkap</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->jeni_alat_tangkap))? $detail_kapal[0]->jenis_alat_tangkap:' ';?></td>
						</tr>
						<tr>
							<td class="garis">No SIUP</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->siup))? $detail_kapal[0]->siup:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Nama KUB Pemilik</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->kub_penerima))? $detail_kapal[0]->kub_penerima:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Nama Ketua KUB</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->ketua_kub))? $detail_kapal[0]->ketua_kub:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Alamat</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->alamat))? $detail_kapal[0]->alamat:' ';?></td>						
						</tr>
							
					</table>
				</div>
				<div class="kiri">
				<p style="font-weight:bold;">Data Operasional & Indikator Hasil</p>
					<hr>
					<table>
					
						<tr>
							<td  class="garis" style="width:210px">Mulai Beroperasi</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->tahun_operasi))? $detail_kapal[0]->tahun_operasi:' ';?></td>
						</tr>
						
						<tr>
							<td class="garis">Pelabuhan Pangkalan</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->pelabuhan_pangkalan))? $detail_kapal[0]->pelabuhan_pangkalan:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Jumlah ABK</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->jumlah_abk))? $detail_kapal[0]->jumlah_abk:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Rata-Rata Kebutuhan BBM/Trip</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->avg_bbm))? $detail_kapal[0]->avg_bbm:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Rata-Rata Produksi/Trip</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->avg_produksi))? $detail_kapal[0]->avg_produksi:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Rata-Rata Biaya Operasional/Trip</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->avg_operasional))? $detail_kapal[0]->avg_operasional:' ';?></td>
						</tr>
						<tr>
							<td class="garis">Rata-Rata Pendapatan/Trip</td>
							<td class="garis">:</td>
							<td class="garis"><?php echo (isset($detail_kapal[0]->avg_pendapatan))? $detail_kapal[0]->avg_pendapatan:' ';?></td>
						</tr>
					</table>
					
				</div>
			</div>
		</div>
		</div>
	</body>
</html>