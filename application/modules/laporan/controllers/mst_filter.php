<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_filter extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_propinsi_json()
	{
		$this->load->model('mdl_propinsi');

		$list_opsi = $this->mdl_propinsi->list_propinsi();

		echo json_encode($list_opsi);
	}

	public function list_propinsi_array()
	{
		$this->load->model('mdl_propinsi');
		
		$list_opsi = $this->mdl_propinsi->list_propinsi();
		
		return $list_opsi;
	}

	public function list_kapal_json()
	{
		$this->load->model('mdl_inka_mina');

		$list_opsi = $this->mdl_inka_mina->list_kapal();

		echo json_encode($list_opsi);
	}

	public function list_kapal_array()
	{
		$this->load->model('mdl_inka_mina');
		
		$list_opsi = $this->mdl_inka_mina->list_kapal();
		
		return $list_opsi;
	}
}
?>