<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_inka_mina extends CI_Model
{
    private $db_kapi;

    function __construct()
    {
        $this->db_kapi = $this->load->database('default', TRUE);

    }


    public function list_inka_mina()
    {
        $query = ' SELECT * FROM mst_inka_mina WHERE mst_inka_mina.aktif = "Ya" ';

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi()
    {
        $query = "SELECT id_kapal AS id, nama_kapal as text FROM mst_inka_mina";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db_kapi->insert('mst_inka_mina', $data);
    }

    public function update($data)
    {
        $this->db_kapi->where('id_kapal', $data['id_kapal']);
        $query = $this->db_kapi->update('mst_inka_mina',$data);

        if($this->db_kapi->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detil_kapal($id)
    {
        $sql = "SELECT *
                    FROM mst_inka_mina
                    WHERE mst_inka_mina.id_kapal = $id ";

        $run_query = $this->db_kapi->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {

        $sql = " UPDATE kapi_monev.mst_inka_mina SET aktif='Tidak' WHERE id_kapal=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    public function from_dss()
    {
        $query = "";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
        $query = "";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}