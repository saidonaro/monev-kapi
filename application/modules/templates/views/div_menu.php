<!-- mulai #kapinav -->
<div id="kapinav">

	<div class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
          	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
          	</button>
          	<a class="navbar-brand" href="index.php" title="Aplikasi Pemantauan Dan Evaluasi Sarana Penangkapan Ikan">KAPI</a>
        </div>
        <div class="navbar-collapse">
          <ul class="nav navbar-nav">
          	<?php foreach ($menus as $index => $values): ?>
          		<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php 
						echo $index;
						if(is_array($values))
						{
							echo "<b class='caret'></b>";
						}
					 ?> 
					</a>
					<?php if( is_array($values) )
					{
					?>
						<ul class="dropdown-menu">
							<?php
							    $jum_dropdown = count($values);
							    $dropdown_counter = 1;
								foreach ($values as $index_2 => $link): ?>
								
									<li><a href="<?php echo base_url().$link ?>"><?php echo $index_2; ?></a></li>
									
							<?php 
							if($dropdown_counter < $jum_dropdown)
							{
								echo "<li class='divider'></li>";
								$dropdown_counter++;
							}
								endforeach ?>
					<?php
					}
					?>
						</ul>	
					
				</li>
          	<?php endforeach ?>
          </ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="#">Pengaturan</a></li>
		</ul>
		</div><!--/.nav-collapse -->
      </div>
    </div>
</div>
<!-- akhir #kapinav -->
