<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_inka_mina extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			$this->load->model('mdl_inka_mina');	

		}

	public function list_inka_mina_json()
	{
		// $this->load->model('mdl_inka_mina');

		$list_opsi = $this->mdl_inka_mina->list_opsi();

		echo json_encode($list_opsi);
	}

	public function detail_inka_mina_json()
	{
		$id_kapal = $this->input->get('id_kapal');	

		$detil_kapal = $this->mdl_inka_mina->detil_kapal($id_kapal);

		echo json_encode($detil_kapal);
	}

	public function list_inka_mina_array()
	{
		// $this->load->model('mdl_inka_mina');

		$list_opsi = $this->mdl_inka_mina->list_opsi();

		return $list_opsi;
	}
	
}
?>