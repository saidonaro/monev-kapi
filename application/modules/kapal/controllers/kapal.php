<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kapal extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_kapal');
	}
	
	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_kapal'] = $this->mdl_kapal->list_inka_mina();
		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$views = 'tabel_kapal';
		$labels = 'view_kapal_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function entry()
	{
		$data['submit_form'] = 'kapal/kapal/input';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$views = 'form_entry_kapal';
		$labels = 'form_kapal';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$array_input['id_pengguna_buat'] = 7;
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');

		/* Awal Program Upload */

		// Ambil nama kapal untuk bikin nama file gambar yang diupload
		$nama_kapal = $array_input['nama_kapal'];
		$nama_kapal = preg_replace("/[^A-Za-z0-9 ]/", '', $nama_kapal);
		$nama_kapal = str_replace(" ", "_", $nama_kapal);

		// Timestamp supaya nama file unique
		$getdate = new DateTime();
		$time_upload = $getdate->getTimestamp();

		// Ambil path ke folder uploads yang diset di custom constants
		$assets_paths = $this->config->item('assets_paths');

		// Rules upload nya di set di custom constants
		// $rules = $this->config->item('upload_rules');

		// Persiapkan config untuk upload
		$config['upload_path'] = 'uploads';  // Set path tempat file disimpan di server

		// Set rules ini dari custom constants
		$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
		$config['max_size']	= '2048'; 
		$config['max_width']  = '2048';
		$config['max_height']  = '1536';
		$this->load->library('upload', $config);

		
		//proses upload foto proses pembuatan
		if( $_FILES['foto_pembuatan'] !== UPLOAD_ERR_NO_FILE) {
			$foto_pembuatan = 'pembuatan_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_pembuatan; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('foto_pembuatan') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_pembuatan'] = $foto_pembuatan;
			}
			$this->load->library('upload', $config, FALSE);
		} 
		else {
			$array_input['foto_pembuatan'] = '';
		}

		//proses upload foto proses sea trial
		if( $_FILES['foto_sea_trial'] !== UPLOAD_ERR_NO_FILE) {
			$foto_sea_trial = 'sea_trial_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_sea_trial; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('foto_sea_trial') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_sea_trial'] = $foto_sea_trial;
			}
		} 
		else {
			$array_input['foto_sea_trial'] = '';
		}

		//proses upload foto orpasional
		if( $_FILES['foto_oprasional'] !== UPLOAD_ERR_NO_FILE) {
			$foto_oprasional = 'oprasional_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_oprasional; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_oprasional') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_oprasional'] = $foto_oprasional;
			}
		} 
		else {
			$array_input['foto_oprasional'] = '';
		}

		//proses upload foto siup1
		if( $_FILES['foto_siup1'] !== UPLOAD_ERR_NO_FILE) {
			$foto_siup1 = 'siup1_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_siup1; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_siup1') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_siup1'] = $foto_siup1;
			}
		} 
		else {
			$array_input['foto_siup1'] = '';
		}

		//proses upload foto siup2
		if( $_FILES['foto_siup2'] !== UPLOAD_ERR_NO_FILE) {
			$foto_siup2 = 'siup2_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_siup2; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_siup2') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_siup2'] = $foto_siup2;
			}
		} 
		else {
			$array_input['foto_siup2'] = '';
		}

		//proses upload foto bast1
		if( $_FILES['foto_bast1'] !== UPLOAD_ERR_NO_FILE) {
			$foto_bast1 = 'bast1_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_bast1; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_bast1') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_bast1'] = $foto_bast1;
			}
		} 
		else {
			$array_input['foto_bast1'] = '';
		}

		//proses upload foto bast2
		if( $_FILES['foto_bast2'] !== UPLOAD_ERR_NO_FILE) {
			$foto_bast2 = 'bast2_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_bast2; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_bast2') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_bast2'] = $foto_bast2;
			}
		} 
		else {
			$array_input['foto_bast2'] = '';
		}
		if( $this->mdl_kapal->input($array_input) ){
			$url = base_url('kapal/kapal');
			redirect($url);
		}else{
			$url = base_url('kapal/kapal');
			redirect($url);
		}
	}

	public function edit($id_record)
	{

		$get_detail = $this->mdl_kapal->detil_kapal($id_record);

		if( !$get_detail )
		{
			$data['list_kapal'] = 'Data tidak ditemukan';
		}else{
			$data['list_kapal'] = $get_detail;
		}

		$data['submit_form'] = 'kapal/kapal/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$views = 'form_entry_kapal';
		$labels = 'form_kapal';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);

		/* Awal Program Upload */

		// Ambil nama kapal untuk bikin nama file gambar yang diupload
		$nama_kapal = $array_input['nama_kapal'];
		$nama_kapal = preg_replace("/[^A-Za-z0-9 ]/", '', $nama_kapal);
		$nama_kapal = str_replace(" ", "_", $nama_kapal);

		// Timestamp supaya nama file unique
		$getdate = new DateTime();
		$time_upload = $getdate->getTimestamp();

		// Ambil path ke folder uploads yang diset di custom constants
		$assets_paths = $this->config->item('assets_paths');

		// Rules upload nya di set di custom constants
		// $rules = $this->config->item('upload_rules');

		// Persiapkan config untuk upload
		$config['upload_path'] = 'uploads';  // Set path tempat file disimpan di server

		// Set rules ini dari custom constants
		$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
		$config['max_size']	= '2048'; 
		$config['max_width']  = '2048';
		$config['max_height']  = '1536';
		$this->load->library('upload', $config);

		// var_dump( $_FILES['foto_pembuatan']); 
		//proses upload foto proses pembuatan
		if( $_FILES['foto_pembuatan']['error'] !== UPLOAD_ERR_NO_FILE) {
			//echo 'manggg';
			$foto_pembuatan = 'pembuatan_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_pembuatan; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('foto_pembuatan') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_pembuatan'] = $foto_pembuatan;
			}
			$this->load->library('upload', $config, FALSE);
		} 
		else {
			echo 'makan';
			unset($array_input['foto_pembuatan']);
		}

		//proses upload foto proses sea trial
		if( $_FILES['foto_sea_trial']['error'] !== UPLOAD_ERR_NO_FILE) {
			$foto_sea_trial = 'sea_trial_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_sea_trial; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('foto_sea_trial') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_sea_trial'] = $foto_sea_trial;
			}
		} 
		else {
			unset($array_input['foto_sea_trial']);
		}

		//proses upload foto orpasional
		if( $_FILES['foto_oprasional']['error'] !== UPLOAD_ERR_NO_FILE) {
			$foto_oprasional = 'oprasional_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_oprasional; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_oprasional') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_oprasional'] = $foto_oprasional;
			}
		} 
		else {
			unset($array_input['foto_oprasional']);
		}

		//proses upload foto siup1
		if( $_FILES['foto_siup1']['error'] !== UPLOAD_ERR_NO_FILE) {
			$foto_siup1 = 'siup1_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_siup1; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_siup1') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_siup1'] = $foto_siup1;
			}
		} 
		else {
			unset($array_input['foto_siup1']);
		}

		//proses upload foto siup2
		if( $_FILES['foto_siup2']['error'] !== UPLOAD_ERR_NO_FILE) {
			$foto_siup2 = 'siup2_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_siup2; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_siup2') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_siup2'] = $foto_siup2;
			}
		} 
		else {
			unset($array_input['foto_siup2']);
		}

		//proses upload foto bast1
		if( $_FILES['foto_bast1']['error'] !== UPLOAD_ERR_NO_FILE) {
			$foto_bast1 = 'bast1_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_bast1; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_bast1') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_bast1'] = $foto_bast1;
			}
		} 
		else {
			unset($array_input['foto_bast1']);
		}

		//proses upload foto bast2
		if( $_FILES['foto_bast2']['error'] !== UPLOAD_ERR_NO_FILE) {
			$foto_bast2 = 'bast2_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_bast2; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_bast2') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_bast2'] = $foto_bast2;
			}
		} 
		else {
			unset($array_input['foto_bast2']);
		}

		$array_to_edit = $array_input;

		if( $this->mdl_kapal->update($array_to_edit) ){
			$url = base_url('kapal/kapal');
			redirect($url);
			// echo $this->db->last_query();

		}else{
			$url = base_url('kapal/kapal');
			redirect($url);
			// echo $this->db->last_query();
		}

		// var_dump($array_to_edit);
	}

	public function delete($id)
    {
        $this->mdl_kapal->delete($id);

        $url = base_url('kapal/kapal');
        redirect($url);
    }

    public function exceltest()
	{
		echo "makan";
		require_once APPPATH."/third_party/tbs_plugin_opentbs_1.8.0/demo/tbs_class.php";
		require_once APPPATH."/third_party/tbs_plugin_opentbs_1.8.0/tbs_plugin_opentbs.php";
		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); 
		$TBS->ResetVarRef(false);
		
		$data = array();
		$data['kode_puskesmas'] = "3216081202";
		$data['nampus'] = "MEKARSARI";
		$data['kecamatan'] = "TAMBUN SELATAN";
		$data['ada'] = "";
		$data['lapor'] = "";
		$data['kab'] = "BEKASI";
		$data['propinsi'] = "JAWA BARAT";
		$data['hal'] = "";
		$data['tahun'] = "2013";
		
		$TBS->VarRef = $data;
		$TBS->LoadTemplate($_SERVER['DOCUMENT_ROOT'].'/monev-kapi/assets/tamplate_excel/promkes-phbs.xlsx');
		$list = array();
		$list[] = array('no' => 1, 'desa' => 'LEMBANGUN', 'f1' => 21,'f2' => 1,'f3' => 33,'f4' => 34);
		$list[] = array('no' => 2, 'desa' => 'MEKAR', 'f1' => 21,'f2' => 1,'f3' => 33,'f4' => 34);
		$list[] = array('no' => 3, 'desa' => 'CICENDOL', 'f1' => 21,'f2' => 1,'f3' => 33,'f4' => 34);
		$total = array(array('t1' => 21,'t2' => 1,'t3' => 33,'t4' => 34.5));
		$TBS->MergeBlock('counter,counter2', 'num', 4);
		$TBS->MergeBlock('input', $list);
		$TBS->MergeBlock('total', $total);
		// $TBS->Show();
		$TBS->PlugIn(OPENTBS_DEBUG_INFO, TRUE);
		$TBS->Show(OPENTBS_DOWNLOAD, "hasil.xlsx");
		// $this->load->view('testing');
	}

	public function exceltest2()
	{
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		//$array_test = array
		for ($index=0; $index < 10 ; $index++) { 
			$this->excel->getActiveSheet()->setCellValue('A'.$index ,'Baris Ke: '.$index);
		}
		
		 
		$filename='just_some_random_name.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		             
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */