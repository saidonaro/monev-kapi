<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_dpi extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_dpi_json()
	{
		$this->load->model('mdl_dpi');

		$list_opsi = $this->mdl_dpi->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_dpi_array()
	{
		$this->load->model('mdl_dpi');

		$list_opsi = $this->mdl_dpi->list_opsi();

		return $list_opsi;
	}
	
}
?>