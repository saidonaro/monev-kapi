<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produksi extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_produksi');
	}

	
	public function index()
	{

		// $data['labels'] =  Array(
		// 							'form_kapal' => $this->config->item('form_kapal')
		// 						);
		// echo Modules::run('templates/page/v_form', //tipe template
		// 					'produksi', //nama module
		// 					'index', //nama file view
		// 					'', //dari labels.php
		// 					'', //plugin javascript khusus untuk page yang akan di load
		// 					'', //file css khusus untuk page yang akan di load
		// 					$data); //array data yang akan digunakan di file view

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		//$data['list_kapal'] = $this->mdl_produksi->list_inka_mina();
		// $data['labels'] =  Array(
		//  							'form_produksi' => $this->config->item('form_produksi')
		//  						);
		$data['list_produksi'] = $this->mdl_produksi->list_produksi();
		$template = 'templates/page/v_form';
		$modules = 'produksi';
		$views = 'tabel_produksi';
		$labels = 'view_produksi_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function entry()
	{
		$data['submit_form'] = 'produksi/produksi/input';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js','jquery.validationEngine.js', 'jquery.validationEngine-en.js' );
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'produksi';
		$views = 'form_entry_produksi';
		$labels = 'form_produksi';

		//$data['list_produksi'] = $this->mdl_produksi->list_produksi();

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}


	public function input()
	{
		$this->load->model('mdl_inka_mina');

		$array_input = $this->input->post(NULL, TRUE);
		$array_input['id_jenis_ikan'] = implode(',' ,  $array_input['id_jenis_ikan']);
		$array_input['pendapatan_bersih'] = $array_input['nilai_pendapatan'] - $array_input['biaya_operasional'];
		
		$array_input['pendapatan_abk'] = ($array_input['pendapatan_bersih'] - $array_input['dana_simpanan_kub']) / $array_input['jumlah_abk'];
		
		$detail_kapal = $this->mdl_inka_mina->detil_kapal($array_input['id_kapal']);
		$array_input['produktivitas_kapal'] = ($array_input['jml_ikan']/1000)/$detail_kapal->gt;
		// echo $array_input['produktivitas_kapal'];
		unset($array_input['gt']);
		if( $this->mdl_produksi->input($array_input) ){
			$url = base_url('produksi/produksi');
			redirect($url);
		}else{
			$url = base_url('produksi/produksi');
			redirect($url);
		}




	}

	public function edit($id_record)
	{

		$get_detail = $this->mdl_produksi->detil_produksi($id_record);

		if( !$get_detail )
		{
			$data['list_produksi'] = 'Data tidak ditemukan';
		}else{
			$data['list_produksi'] = $get_detail;
		}

		$data['submit_form'] = 'produksi/produksi/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'produksi';
		$views = 'form_entry_produksi';
		$labels = 'form_produksi';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$this->load->model('mdl_inka_mina');

		$array_input = $this->input->post(NULL, TRUE);
		$array_input['id_jenis_ikan'] = implode(',' ,  $array_input['id_jenis_ikan']);
		$array_input['pendapatan_bersih'] = $array_input['nilai_pendapatan'] - $array_input['biaya_operasional'];
		
		$array_input['pendapatan_abk'] = ($array_input['pendapatan_bersih'] - $array_input['dana_simpanan_kub']) / $array_input['jumlah_abk'];
		
		$detail_kapal = $this->mdl_inka_mina->detil_kapal($array_input['id_kapal']);
		$array_input['produktivitas_kapal'] = ($array_input['jml_ikan']/1000)/$detail_kapal->gt;

		$array_to_edit = $array_input;
		if( $this->mdl_produksi->update($array_to_edit) ){
			$url = base_url('produksi/produksi');
			redirect($url);

		}else{
			$url = base_url('produksi/produksi');
			redirect($url);
		}
	}

	public function delete($id)
    {
        $this->mdl_produksi->delete($id);

        $url = base_url('produksi/produksi');
        redirect($url);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */