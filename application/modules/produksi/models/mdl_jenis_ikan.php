<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_jenis_ikan extends CI_Model
{
	private $db_monev;

    function __construct()
    {
        $this->db_monev = $this->load->database('default', TRUE);
    }

	public function list_opsi()
    {
    	$query = "SELECT id_jenis_ikan AS id, nama_jenis_ikan as text FROM mst_jenis_ikan";

    	$run_query = $this->db_monev->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}