<?php
	$config = Array(
					'app' => Array(
		        					'header_title' => 'Monev Kapi | Aplikasi Monitoring dan Evaluasi Kapal Inka Mina',
		        					'footer_text' => 'Hak cipta © 2013 | <strong>Monev Kapi</strong> | Kementerian Kelautan dan Perikanan <br> Republik Indonesia<br>
														Jl. Medan Merdeka Timur No. 16 Jakarta Pusat. Telp. (021)-3520726 '
		        					),
					'assets_paths' => Array(
	        						'misc_css' => base_url('assets/third_party/css'),
	        						'misc_js' => base_url('assets/third_party/js'),
	        						'misc_sounds' => base_url('assets/third_party/sounds'),
	        						'main_css' => base_url('assets/monevkapi/css'),
	        						'main_js' => base_url('assets/monevkapi/js'),
	        						'kapi_images' => base_url('assets/monevkapi/images'),
	        						'kapo_uploads' => base_url('assets/monevkapi/uploads'),
	        						'mockup_images' => base_url('assets/monevkapi/images/mockup')
	        						),
					'Hari_f'	=> Array('Senin', 'Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'),
					'Hari_s'	=> Array('Sen', 'Sel','Rab','Kam','Jum','Sab','Ming'),
					'Bulan_f' => Array('Januari', 'Februari', 'Maret',
											'April','Mei','Juni','Juli',
												'Agustus','September','Oktober','November','Desember'),
					'Bulan_s'	=> Array('Jan', 'Feb', 'Mar',
											'Apr','Mei','Jun','Jul',
												'Agst','Sept','Okt','Nov','Des')					
					);
?>