<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_jenis_ikan extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_jenis_ikan_json()
	{
		$this->load->model('mdl_jenis_ikan');

		$list_opsi = $this->mdl_jenis_ikan->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_jenis_ikan_array()
	{
		$this->load->model('mdl_jenis_ikan');

		$list_opsi = $this->mdl_jenis_ikan->list_opsi();

		return $list_opsi;
	}
	
}
?>