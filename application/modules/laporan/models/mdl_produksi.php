<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_produksi extends CI_Model
{
	//private $db_dss;
	private $db_kapi;

    function __construct()
    {
        //$this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);

    }

   public function detail_list_produksi($id)
    {
        
        $this->db_kapi->select('trs_produksi.id_wpp, nama_wpp, gt, nama_kapal,
                                tgl_berangkat, jml_hari_operasi,
                                jml_ikan, nilai_pendapatan,
                                nama_jenis_ikan, kebutuhan_bbm,
                                biaya_operasional, jumlah_abk,
                                pendapatan_bersih,
                                dana_simpanan_kub, pendapatan_abk,
                                produktivitas_kapal, keterangan');
        $this->db_kapi->from('trs_produksi');
        $this->db_kapi->join('mst_inka_mina', 'mst_inka_mina.id_kapal = trs_produksi.id_kapal', 'left');
        $this->db_kapi->join('(SELECT   id_produksi, 
                                        group_concat(nama_jenis_ikan separator ", ") AS nama_jenis_ikan
                                FROM trs_produksi
                                LEFT JOIN mst_jenis_ikan 
                                    ON find_in_set(mst_jenis_ikan.id_jenis_ikan, trs_produksi.id_jenis_ikan)
                                GROUP BY id_produksi) jenis_ikan', 
                                'jenis_ikan.id_produksi = trs_produksi.id_produksi', 'left');
        $this->db_kapi->join('mst_wpp', 'mst_wpp.id_wpp = trs_produksi.id_wpp', 'left');
        $this->db_kapi->join('mst_dpi', 'mst_dpi.id_dpi = trs_produksi.id_dpi', 'left');
        $this->db_kapi->where('trs_produksi.id_kapal', $id);
        $run_query = $this->db_kapi->get();
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = FALSE;
        }
        return $result;
    }
}