<!-- filter propinsi -->
<?php
	echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
	// print_r(Modules::run('laporan/mst_filter/list_propinsi_array'));
	$attr_provinsi = array( 'name' => 'provinsi',
                                  'label' => 'Provinsi',
                                  'opsi' => Modules::run('laporan/mst_filter/list_propinsi_array'),
                                  'all' => TRUE,
                                  'value' => $id_propinsi
                    );
          echo $this->mkform->input_select2($attr_provinsi);
?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Filter</button>
              <button type="submit" class="btn btn-primary" name="export">Export</button>
            </div>
          </div>
  </div>
</div>


<!-- tabel laporan  -->
<?php
	$template = array( "table_open" => "<table id='table_daftar_kapal' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading(	'NO.',
								'Provinsi',
								'Kab/Kota',
								'Tahun Pembuatan (10/11/12)',
								'Nama Kapal',
								'Tanda Selar',
								'KUB Penerima',
								'Ketua',
								'Anggota (orang)',
								'Kontak KUB',
								'Alamat',
								'Sumber Anggaran (TP/DK)',
								'Bahan Kapal',
								'GT',
								'P (m)',
								'L (m)',
								'D (m)',
								'Mesin',
								'Daya (PK/HP)',
								'Pelabuhan Pangkalan',
								'Jenis Alat Tangkap',
								'Grosse Akte/Buku Kapal',
								'SUP',
								'SIPI',
								'Total Produksi (kg)',
								'Jumlah Trip Selama ini (kali)',
								'Rata-rata Jumlah hari/trip (Hari)',
								'Ratarata Produksi (Kg/Kapal/Trip)',
								'Rata-rata Kebutuhan BBM/Trip (Tonk/Kapal/Trip)',
								'Rata-rata Operasional/Trip (RP/Kapal/Trip)',
								'Rata-rata Pendapatan Bersih (Rp/Kapal/Trip)',
								'Kontraktor Pelaksanaan Pembangunan',
								'Lokasi Pembangunan Kapal'
							);
	if($list_kapal){
		$index = 1;
		foreach ($list_kapal as $item) {
			$this->table->add_row(
				$index,
				$item->nama_propinsi,
				$item->nama_kabupaten_kota,
				$item->tahun_pembuatan,
				$item->nama_kapal,
				$item->tanda_selar,
				$item->kub_penerima,
				$item->ketua,
				($item->anggota!=0 ? $item->anggota : ''),
				$item->kontak,
				$item->alamat,
				$item->sumber_anggaran,
				$item->bahan_kapal,
				$item->gt,
				($item->panjang_kapal!=0 ? $item->panjang_kapal : ''),
				($item->lebar_kapal!=0? $item->lebar_kapal : ''),
				($item->dalam_kapal!=0? $item->dalam_kapal : ''),
				$item->mesin,
				($item->daya!=0? $item->daya : ''),
				$item->pelabuhan_pangkalan,
				$item->jenis_alat_tangkap,
				$item->gross_akte,
				$item->siup,
				$item->sipi,
				$item->jml_produksi,
				$item->jml_opreasi,
				$item->avg_hari_operasi!==0?(int) $item->avg_hari_operasi:'',
				$item->avg_produksi,
				$item->avg_bbm,
				$item->avg_operasional,
				$item->avg_pendapatan,
				$item->kontraktor_pembangunan,
				$item->lokasi_pembangunan
				);
			$index++;	
		}
	}
	$table_list_kapal = $this->table->generate();
?>
<!-- TAMPIL DATA -->
	<div style="width:100%;border:1px solid #ccc;overflow:auto;">
	<!-- <div style="height:800px;width:100%;"> -->
		<?php
			echo $table_list_kapal;
		?>
	</div>

<script>
	$(document).ready( function () {
		$('#table_daftar_kapal').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"bFilter": true,
	        "bAutoWidth": true
		} );
	} );
</script>