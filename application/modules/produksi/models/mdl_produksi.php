<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_produksi extends CI_Model
{
	//private $db_dss;
	private $db_kapi;

    function __construct()
    {
        //$this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);

    }

   public function list_produksi()
    {
        $this->db_kapi->select('trs_produksi.id_produksi,
                                nama_kapal,
                                jenis_alat_tangkap as nama_alat_tangkap,
                                nama_wpp,
                                nama_dpi,
                                gt,
                                tgl_berangkat,
                                jml_hari_operasi,
                                jml_ikan,
                                nilai_pendapatan,
                                jenis_ikan.id_jenis_ikan,
                                kebutuhan_bbm,
                                biaya_operasional,
                                jumlah_abk,
                                pendapatan_bersih,
                                dana_simpanan_kub,
                                pendapatan_abk,
                                produktivitas_kapal,
                                keterangan');
        $this->db_kapi->from('trs_produksi');
        $this->db_kapi->join('mst_inka_mina', 'mst_inka_mina.id_kapal = trs_produksi.id_kapal', 'inner');
        $this->db_kapi->join('mst_wpp', 'mst_wpp.id_wpp = trs_produksi.id_wpp', 'left');
        $this->db_kapi->join('mst_dpi', 'mst_dpi.id_dpi = trs_produksi.id_dpi', 'left');
        $this->db_kapi->join('(SELECT   id_produksi, 
                                        group_concat(nama_jenis_ikan separator ", ") AS id_jenis_ikan
                                FROM trs_produksi
                                LEFT JOIN mst_jenis_ikan 
                                    ON find_in_set(mst_jenis_ikan.id_jenis_ikan, trs_produksi.id_jenis_ikan)
                                GROUP BY id_produksi) jenis_ikan', 
                                'jenis_ikan.id_produksi = trs_produksi.id_produksi', 'left');
        $this->db_kapi->where('trs_produksi.aktif', "Ya");
        $run_query = $this->db_kapi->get();                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    


    public function input($data)
    {
        $this->db_kapi->insert('trs_produksi', $data);
        if($this->db_kapi->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function update($data)
    {
        $this->db_kapi->where('id_produksi', $data['id_produksi']);
        $query = $this->db_kapi->update('trs_produksi',$data);

        if($this->db_kapi->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function delete($id)
    {

        $sql = " UPDATE trs_produksi SET aktif='Tidak' WHERE id_produksi=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    public function detil_produksi($id)
    {
       $this->db_kapi->select('trs_produksi.id_produksi,
                                trs_produksi.id_kapal,
                                nama_kapal,
                                id_alat_tangkap,
                                nama_wpp,
                                trs_produksi.id_wpp,
                                nama_dpi,
                                trs_produksi.id_dpi,
                                gt,
                                tgl_berangkat,
                                jml_hari_operasi,
                                jml_ikan,
                                nilai_pendapatan,
                                trs_produksi.id_jenis_ikan,
                                kebutuhan_bbm,
                                biaya_operasional,
                                jumlah_abk,
                                pendapatan_bersih,
                                dana_simpanan_kub,
                                pendapatan_abk,
                                produktivitas_kapal,
                                keterangan');
        $this->db_kapi->from('trs_produksi');
        $this->db_kapi->join('mst_inka_mina', 'mst_inka_mina.id_kapal = trs_produksi.id_kapal', 'inner');
        $this->db_kapi->join('mst_wpp', 'mst_wpp.id_wpp = trs_produksi.id_wpp', 'left');
        $this->db_kapi->join('mst_dpi', 'mst_dpi.id_dpi = trs_produksi.id_dpi', 'left');
        $this->db_kapi->join('(SELECT   id_produksi, 
                                        group_concat(nama_jenis_ikan separator ", ") AS id_jenis_ikan
                                FROM trs_produksi
                                LEFT JOIN mst_jenis_ikan 
                                    ON find_in_set(mst_jenis_ikan.id_jenis_ikan, trs_produksi.id_jenis_ikan)
                                GROUP BY id_produksi) jenis_ikan', 
                                'jenis_ikan.id_produksi = trs_produksi.id_produksi', 'left');
        $this->db_kapi->where('trs_produksi.aktif', "Ya");
        $this->db_kapi->where('trs_produksi.id_produksi', "$id");
        $run_query = $this->db_kapi->get();                           

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }


	public function from_dss()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }


}