<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_propinsi extends CI_Model
{
	private $db_monev;

    function __construct()
    {
        $this->db_kapi = $this->load->database('default', TRUE);
    }

	public function list_propinsi()
    {
    	$this->db_kapi->select('id_propinsi AS id, nama_propinsi AS text');
        $this->db_kapi->from('mst_propinsi');
        $run_query = $this->db_kapi->get();
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}