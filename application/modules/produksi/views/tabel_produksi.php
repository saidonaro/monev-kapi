<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_produksi' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_produksi){
		foreach ($list_produksi as $item) {
			$link_edit = '<a class="btn btn-warning" href="'.base_url('produksi/produksi/edit/'.$item->id_produksi).'">Edit</a>';
			$link_delete = '<a class="btn btn-danger" href="'.base_url('produksi/produksi/delete/'.$item->id_produksi).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								$item->nama_kapal,
								$item->nama_alat_tangkap,
								$item->nama_wpp,
								$item->nama_dpi,
								$item->gt,
								$item->tgl_berangkat,
								$item->jml_hari_operasi,
								$item->jml_ikan,
								$item->nilai_pendapatan,
								$item->id_jenis_ikan,
								$item->kebutuhan_bbm,
								//'P: '.$item->panjang_kapal.'<br>L: '.$item->lebar_kapal.'<br>D: '.$item->dalam_kapal,
								$item->biaya_operasional,
								$item->jumlah_abk,
								$item->pendapatan_bersih,
								$item->dana_simpanan_kub,
								$item->pendapatan_abk,
								$item->produktivitas_kapal,
								$item->keterangan,
								//$item->aksi,
								$link_edit.' '.$link_delete
								);
			$counter++;
		}
	}

	$table_list_produksi = $this->table->generate();
?>

<!-- TAMPIL DATA -->
	<div style="width:100%;overflow:auto;">
		<?php
			echo $table_list_produksi;
		?>
	</div>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_produksi').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			
		} );
	} );
</script>