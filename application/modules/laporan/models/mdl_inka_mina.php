<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_inka_mina extends CI_Model
{
	//private $db_dss;
	private $db_kapi;

    function __construct()
    {
        //$this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);

    }

    public function detail_kapal($id)
    {
        $this->db_kapi->select('mst_inka_mina.*, produksi.*');
        $this->db_kapi->from('mst_inka_mina');
        $this->db_kapi->where('mst_inka_mina.id_kapal',$id);
        $this->db_kapi->join('(SELECT id_kapal, MAX( jumlah_abk ) AS jumlah_abk,  MIN( YEAR( tgl_berangkat ) )AS tahun_operasi,
                                    COUNT(*) AS jml_opreasi,  
                                    AVG(jml_hari_operasi) AS avg_hari_operasi,
                                    AVG(jml_ikan) AS avg_produksi,
                                    AVG(kebutuhan_bbm) AS avg_bbm,
                                    AVG(biaya_operasional) AS avg_operasional,
                                    AVG(pendapatan_bersih) AS avg_pendapatan
                                FROM trs_produksi 
                                WHERE aktif = "Ya"
                                GROUP BY id_kapal) produksi', 'produksi.id_kapal = mst_inka_mina.id_kapal', 'left');     

        $run_query = $this->db_kapi->get();
        // $str = $this->db_kapi->last_query(); 
        // echo $str;
        // die;
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
         return $result;
    }

    public function list_kapal()
    {
        $this->db_kapi->select('id_kapal AS id, nama_kapal AS text');
        $this->db_kapi->from('mst_inka_mina');
        $run_query = $this->db_kapi->get();

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
         return $result;
    }

    public function list_inka_mina($id_propinsi)
    {
        $this->db_kapi->select('mst_inka_mina.*, 
                                nama_kabupaten_kota,
                                mst_kabupaten_kota.id_propinsi,
                                nama_propinsi, 
                                produksi.*');
        $this->db_kapi->from('mst_inka_mina');
        $this->db_kapi->join('mst_kabupaten_kota', 'mst_kabupaten_kota.id_kabupaten_kota = mst_inka_mina.kab_kota', 'left');
        $this->db_kapi->join('mst_propinsi', 'mst_propinsi.id_propinsi = mst_kabupaten_kota.id_propinsi', 'left');
        $this->db_kapi->join('(SELECT id_kapal, SUM(jml_ikan) AS jml_produksi, 
                                    COUNT(*) AS jml_opreasi,  
                                    AVG(jml_hari_operasi) AS avg_hari_operasi,
                                    AVG(jml_ikan) AS avg_produksi,
                                    AVG(kebutuhan_bbm) AS avg_bbm,
                                    AVG(biaya_operasional) AS avg_operasional,
                                    AVG(pendapatan_bersih) AS avg_pendapatan
                                FROM trs_produksi 
                                GROUP BY id_kapal) produksi', 'produksi.id_kapal = mst_inka_mina.id_kapal', 'left');
        if ($id_propinsi != 0) {
            $this->db_kapi->where('mst_propinsi.id_propinsi',$id_propinsi);
        }
        
        $run_query = $this->db_kapi->get();
        // $str = $this->db_kapi->last_query(); 
        // echo $str;
        // die;
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
         return $result;
    }
}