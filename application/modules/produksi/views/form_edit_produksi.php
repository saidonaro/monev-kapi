<div class="row">
  <div class="col-lg-12">
          <?php
          //var_dump($list_produksi);
          // $input_hidden  = array('id_kapal' => $list_kapal['id_kapal'] );
          $input_hidden  = array('id_produksi' => $list_produksi['id_produksi'] );

          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $input_hidden);
          ?>

         <?php 
           $attr_nama_kapal = array( 'name' => $form['nama_kapal']['name'],
                          'label' => $form['nama_kapal']['label'],
                          'opsi' => Modules::run('produksi/mst_inka_mina/list_inka_mina_array')
          );          
         echo $this->mkform->input_select2($attr_nama_kapal);
         
          $attr_jenis_alat_tangkap = array( 'name' => $form['jenis_alat_tangkap']['name'],
                                            'label' => $form['jenis_alat_tangkap']['label'],
                                            'opsi' => Modules::run('kapal/mst_alat_tangkap/list_alat_tangkap_array')
                    );
          echo $this->mkform->input_select2($attr_jenis_alat_tangkap);


          $attr_wpp = array( 'name' => $form['wpp']['name'],
                                  'label' => $form['wpp']['label'],
                                  'opsi' => Modules::run('produksi/mst_wpp/list_wpp_array')
                    );
          echo $this->mkform->input_select2($attr_wpp);

          $attr_nama_dpi = array('name' => $form['nama_dpi']['name'],
                                 'label' => $form['nama_dpi']['label'],
                                 'opsi' => Modules::run('produksi/mst_dpi/list_dpi_array')
                    );
          echo $this->mkform->input_select2($attr_nama_dpi);           

          $attr_gt = array( 'name' => $form['gt']['name'],
                                        'label' => $form['gt']['label'],
                                        'value' => $list_produksi['gt']
                    );          
          echo $this->mkform->input_text($attr_gt);  

          $attr_tgl_berangkat = array( 'name' => $form['tgl_berangkat']['name'],
                                        'label' => $form['tgl_berangkat']['label'],
                                        'value'=> $list_produksi['tgl_berangkat']
                    );
          echo $this->mkform->input_text($attr_tgl_berangkat);  

          $attr_jml_hari_operasi = array( 'name' => $form['jml_hari_operasi']['name'],
                                        'label' => $form['jml_hari_operasi']['label'],
                                        'value' => $list_produksi['jml_hari_operasi']
                    );          
          echo $this->mkform->input_text($attr_jml_hari_operasi);  

          $attr_jml_ikan = array( 'name' => $form['jml_ikan']['name'],
                                        'label' => $form['jml_ikan']['label'],
                                        'value' => $list_produksi['jml_ikan']
                    );
          echo $this->mkform->input_text($attr_jml_ikan); 

          $attr_nilai_pendapatan = array( 'name' => $form['nilai_pendapatan']['name'],
                                        'label' => $form['nilai_pendapatan']['label'],
                                        'value' => $list_produksi['nilai_pendapatan']
                    );
          echo $this->mkform->input_text($attr_nilai_pendapatan); 
 

          $attr_jenis_ikan = array( 'name' => $form['jenis_ikan']['name'],
                                  'label' => $form['jenis_ikan']['label'],
                                  'opsi' => Modules::run('produksi/mst_jenis_ikan/list_jenis_ikan_array')
                    );
          echo $this->mkform->input_select2($attr_jenis_ikan);

          $attr_kebutuhan_bbm = array( 'name' => $form['kebutuhan_bbm']['name'],
                                        'label' => $form['kebutuhan_bbm']['label'],
                                        'value' => $list_produksi['kebutuhan_bbm']
                    );
          echo $this->mkform->input_text($attr_kebutuhan_bbm); 

          $attr_biaya_operasional = array( 'name' => $form['biaya_operasional']['name'],
                                        'label' => $form['biaya_operasional']['label'],
                                        'value' => $list_produksi['biaya_operasional']
                    );
          echo $this->mkform->input_text($attr_biaya_operasional); 

          $attr_jumlah_abk = array( 'name' => $form['jumlah_abk']['name'],
                                        'label' => $form['jumlah_abk']['label'],
                                        'value' => $list_produksi['jumlah_abk']
                    );
          echo $this->mkform->input_text($attr_jumlah_abk); 

          $attr_pendapatan_bersih = array( 'name' => $form['pendapatan_bersih']['name'],
                                        'label' => $form['pendapatan_bersih']['label'],
                                        'value' => $list_produksi['pendapatan_bersih']
                    );
          echo $this->mkform->input_text($attr_pendapatan_bersih); 

          $attr_dana_simpanan_kub = array( 'name' => $form['dana_simpanan_kub']['name'],
                                        'label' => $form['dana_simpanan_kub']['label'],
                                        'value' => $list_produksi['dana_simpanan_kub']
                    );
          echo $this->mkform->input_text($attr_dana_simpanan_kub); 

          $attr_pendapatan_abk = array( 'name' => $form['pendapatan_abk']['name'],
                                        'label' => $form['pendapatan_abk']['label'],
                                        'value' => $list_produksi['pendapatan_abk']
                    );
          echo $this->mkform->input_text($attr_pendapatan_abk);

          $attr_produktivitas_kapal = array( 'name' => $form['produktivitas_kapal']['name'],
                                        'label' => $form['produktivitas_kapal']['label'],
                                        'value' => $list_produksi['produktivitas_kapal']
                    );
          echo $this->mkform->input_text($attr_produktivitas_kapal);           

          $attr_keterangan = array( 'name' => $form['keterangan']['name'],
                                        'label' => $form['keterangan']['label'],
                                        'value' => $list_produksi['keterangan']
                    );
          echo $this->mkform->input_text($attr_keterangan);


         ?>
  </div>
</div>   
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>

<div id="modal-search-kapal" class="modal fade" data-width="760">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
        <thead> <th>No.</th> <th>Nama Kapal</th><th>No. SIPI</th><th>Tanggal SIPI</th></thead>
        <tbody></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>  
</div><!-- /.modal -->


<script>
    var set_validation = function() 
    {
      $("#id_no_surat_permohonan").addClass('validate[required]');

      $('#form_entry').validationEngine();
    }//end set_validation

    var typingTimer;                //timer identifier
    var doneTypingInterval = 1500;
    var search_kapal_listener = function () {
   
      function doneTyping(query){
        $("#id_nama_kapal").popover('destroy');
        if(query !== '')
        {
          $("#id_nama_kapal").popover({ content: 'Cari Kapal : <button type="button" class="btn btn-info btn-cari-kapal">'+query+'</button>', html: true, placement: 'top'});
          $("#id_nama_kapal").popover('show');
        }
      }

      $("#id_nama_kapal").keydown(function(){
        clearTimeout(typingTimer);
      });

      $("#id_nama_kapal").keyup(function(){
        var query = $(this).val();
          typingTimer = setTimeout(function(){ doneTyping(query); }, doneTypingInterval);
      });

      function update_result(data)
      {
        // console.log(data);
        $("#modal-search-kapal .modal-title").html('');
        $("#modal-search-kapal .modal-title").text('Pencarian Kapal "'+data.search_like+'" ('+data.filter+'). Ditemukan '+data.jumlah_result+' kapal.');
        $("#modal-search-kapal tbody").html(''); // Kosongin
          if(data.jumlah_result > 0)
          {
            data.result.forEach(function(d, i){
              $("#modal-search-kapal tbody").append('<tr><td>'+(i+1)+'.</td> <td>'+d.nama_kapal+'</td><td>'+d.no_sipi+'</td><td>'+d.tanggal_sipi+' s/d '+d.tanggal_akhir_sipi+'</td>  </tr>');
            });
          }

        $("#modal-search-kapal").modal('show');
      }

      $(".form-group").on("click", ".btn-cari-kapal",function(){
          var query = $(this).text();
          // $("#modal-search-kapal .modal-title").text("Pencarin Kapal : "+query);
              $.ajax({
                dataType: "json",
                url: link_search_kapal, //
                data: { i: 'pusat', q: query}, // 
                success: update_result // 
              });
          
      });
    }//end search_kapal_listener

  s_func.push(set_validation);
  s_func.push(search_kapal_listener);
</script>