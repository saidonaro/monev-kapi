<div class="row">
  <div class="col-lg-12">
          <?php

          //echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
          //var_dump($list_produksi);
          //$input_hidden  = array('gt' => $list_produksi['gt']);

          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
          if(isset($list_produksi->id_produksi)){
            $input_hidden  = array('id_produksi' => $list_produksi->id_produksi );

            echo form_hidden($input_hidden);
          }
          ?>


         <?php

          // $attr_jenis_ikan = array( 'name' => $form['jenis_ikan']['name'],
          //                         'label' => $form['jenis_ikan']['label'],
          //                         'opsi' => Modules::run('produksi/mst_jenis_ikan/list_jenis_ikan_array')
          //           );
          // echo $this->mkform->input_select2($attr_jenis_ikan); 
         $attr_nama_kapal = array( 'name' => $form['id_kapal']['name'],
                          'label' => $form['nama_kapal']['label'],
                          'value' => (isset($list_produksi->id_kapal)? $list_produksi->id_kapal : 0),
                          'opsi' => Modules::run('produksi/mst_inka_mina/list_inka_mina_array')
          );          
         echo $this->mkform->input_select2($attr_nama_kapal);
         
          $attr_jenis_alat_tangkap = array( 'name' => $form['id_alat_tangkap']['name'],
                                            'label' => $form['jenis_alat_tangkap']['label'],
                                            'value' => (isset($list_produksi->id_alat_tangkap)? $list_produksi->id_jenis_ikan : 0),
                                            'opsi' => Modules::run('kapal/mst_alat_tangkap/list_alat_tangkap_array')
                    );
          echo $this->mkform->input_select2($attr_jenis_alat_tangkap);


          $attr_wpp = array( 'name' => $form['id_wpp']['name'],
                                  'label' => $form['wpp']['label'],
                                  'value' => (isset($list_produksi->id_wpp)? $list_produksi->id_wpp : 0),
                                  'opsi' => Modules::run('produksi/mst_wpp/list_wpp_array')
                    );
          echo $this->mkform->input_select2($attr_wpp);

          $attr_daerah_penangkapan = array('name' => $form['id_dpi']['name'],
                                 'label' => $form['nama_dpi']['label'],
                                 'value' => (isset($list_produksi->id_dpi)? $list_produksi->id_dpi : 0),
                                 'opsi' => Modules::run('produksi/mst_dpi/list_dpi_array'),
                                 'placeholder' => ''
                    );
          echo $this->mkform->input_select2($attr_daerah_penangkapan);           

          // $attr_gt = array( 'name' => 'id_gt',
          //                     'value' => '...',
          //                               'label' => $form['gt']['label']
          //           );          
          // echo $this->mkform->input_text($attr_gt);  

          //  $attr_gt = array( 'name' => $form['gt']['name'],
          //                                   'label' => $form['gt']['label'],
          //                                   'value' => (isset($list_produksi->gt)? $list_produksi->gt : '')
          //           );
          // echo $this->mkform->input_text($attr_gt); 

          $tanggal = (isset($list_produksi->tgl_berangkat)? $list_produksi->tgl_berangkat : '');
          $attr_tanggal_berangkat = array(  'mindate' => array('time' => '1 year'),
                                          'default_date' => $tanggal,
                                          'placeholder' => '', 
                                          'name' => $form['tgl_berangkat']['name'],
                                        'label' => $form['tgl_berangkat']['label']
                    );
          echo $this->mkform->input_date($attr_tanggal_berangkat);  

          $attr_jml_hari_operasi = array( 'name' => $form['jml_hari_operasi']['name'],
                                        'label' => $form['jml_hari_operasi']['label'],
                                        'value' => (isset($list_produksi->jml_hari_operasi)? $list_produksi-> jml_hari_operasi : '')
                    );          
          echo $this->mkform->input_text($attr_jml_hari_operasi);  

          $attr_jml_ikan = array( 'name' => $form['jml_ikan']['name'],
                                        'label' => $form['jml_ikan']['label'],
                                        'value' => (isset($list_produksi->jml_ikan)? $list_produksi->jml_ikan : '')
                    );
          echo $this->mkform->input_text($attr_jml_ikan); 

          $attr_nilai_pendapatan = array( 'name' => $form['nilai_pendapatan']['name'],
                                        'label' => $form['nilai_pendapatan']['label'],
                                        'value' => (isset($list_produksi->nilai_pendapatan)? $list_produksi->nilai_pendapatan : '')
                    );
          echo $this->mkform->input_text($attr_nilai_pendapatan); 
 

          $attr_jenis_ikan = array( 'name' => $form['id_jenis_ikan']['name'].'[]',
                                  'label' => $form['jenis_ikan']['label'],
                                  'opsi' => Modules::run('produksi/mst_jenis_ikan/list_jenis_ikan_array'),
                                  'is_multipel' => TRUE,
                                  'value_m' => (isset($list_produksi->id_jenis_ikan)? $list_produksi->id_jenis_ikan : '')
                    );
          echo $this->mkform->input_select2($attr_jenis_ikan);

          $attr_kebutuhan_bbm = array( 'name' => $form['kebutuhan_bbm']['name'],
                                        'label' => $form['kebutuhan_bbm']['label'],
                                        'value' => (isset($list_produksi->kebutuhan_bbm)? $list_produksi->kebutuhan_bbm : '')
                    );
          echo $this->mkform->input_text($attr_kebutuhan_bbm); 

          $attr_biaya_operasional = array( 'name' => $form['biaya_operasional']['name'],
                                        'label' => $form['biaya_operasional']['label'],
                                        'value' => (isset($list_produksi->biaya_operasional)? $list_produksi->biaya_operasional : '')
                    );
          echo $this->mkform->input_text($attr_biaya_operasional); 

          $attr_jumlah_abk = array( 'name' => $form['jumlah_abk']['name'],
                                        'label' => $form['jumlah_abk']['label'],
                                        'value' => (isset($list_produksi->jumlah_abk)? $list_produksi->jumlah_abk : '')
                    );
          echo $this->mkform->input_text($attr_jumlah_abk); 

          // $attr_pendapatan_bersih = array( 'name' => $form['pendapatan_bersih']['name'],
          //                               'label' => $form['pendapatan_bersih']['label']
          //           );
          // echo $this->mkform->input_text($attr_pendapatan_bersih); 

          $attr_dana_simpanan_kub = array( 'name' => $form['dana_simpanan_kub']['name'],
                                        'label' => $form['dana_simpanan_kub']['label'],
                                        'value' => (isset($list_produksi->dana_simpanan_kub)? $list_produksi->dana_simpanan_kub : '')
                    );
          echo $this->mkform->input_text($attr_dana_simpanan_kub); 

          // $attr_pendapatan_abk = array( 'name' => $form['pendapatan_abk']['name'],
          //                               'label' => $form['pendapatan_abk']['label']
          //           );
          // echo $this->mkform->input_text($attr_pendapatan_abk);

          // $attr_produktivitas_kapal = array( 'name' => $form['produktivitas_kapal']['name'],
          //                               'label' => $form['produktivitas_kapal']['label']
          //           );
          // echo $this->mkform->input_text($attr_produktivitas_kapal);           

          $attr_keterangan = array( 'name' => $form['keterangan']['name'],
                                        'label' => $form['keterangan']['label'],
                                        'value' => (isset($list_produksi->keterangan)? $list_produksi->keterangan : '')
                    );
          echo $this->mkform->input_text($attr_keterangan);
         ?>
  </div>
</div>   
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>

<div id="modal-search-kapal" class="modal fade" data-width="760">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
        <thead> <th>No.</th> <th>Nama Kapal</th><th>No. SIPI</th><th>Tanggal SIPI</th></thead>
        <tbody></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>  
</div><!-- /.modal -->


<script>
    var set_validation = function() 
    {
      $("#id_no_surat_permohonan").addClass('validate[required]');

      // $('#form_entry').validationEngine();
    }//end set_validation

    var set_detail_kapal = function(data)
    {
      console.log(data.jenis_alat_tangkap );
      console.log(data.gt);

      $("#id_id_alat_tangkap").select2("val", data.jenis_alat_tangkap);
      $("#id_gt").val(data.gt);
      //$("#id_gt").text(data.gt);
    }  
    var get_detail_kapal = function(a_id_kapal)
    {
      $.ajax({
        type: "GET",
        data: {id_kapal : a_id_kapal},
        dataType: "json",
        url: link_detail_kapal_inka_mina,
        success: set_detail_kapal

      });
    }

    var listener_pilih_kapal = function()
    {
      $('#id_id_kapal').on("change", function(){
          var id_kapal = $('#id_id_kapal').select2("val");
          get_detail_kapal(id_kapal);
      });
    }
  s_func.push(listener_pilih_kapal);
  s_func.push(set_validation);
</script>