<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_kapal extends CI_Model
{
	private $db_kapi;

    function __construct()
    {
        $this->db_kapi = $this->load->database('default', TRUE);

    }

    public function list_inka_mina()
    {

        // $query = ' SELECT * FROM mst_inka_mina WHERE mst_inka_mina.aktif = "Ya" ';
        $this->db_kapi->select('id_kapal,
                                mst_propinsi.nama_propinsi as provinsi,
                                mst_kabupaten_kota`.`nama_kabupaten_kota`  as kab_kota,
                                tahun_pembuatan,
                                nama_kapal,
                                tanda_selar,
                                kub_penerima,
                                ketua,
                                anggota,
                                kontak,
                                alamat,
                                sumber_anggaran,
                                bahan_kapal,
                                gt,
                                panjang_kapal,
                                lebar_kapal,
                                dalam_kapal,
                                mesin,
                                daya,
                                pelabuhan_pangkalan,
                                jenis_alat_tangkap,
                                gross_akte,
                                siup,
                                sipi,
                                kontraktor_pembangunan,
                                lokasi_pembangunan');
        $this->db_kapi->from('mst_inka_mina');
        $this->db_kapi->join('mst_kabupaten_kota', 'id_kabupaten_kota = kab_kota', 'left');
        $this->db_kapi->join('mst_propinsi', 'mst_propinsi.id_propinsi = mst_kabupaten_kota.id_propinsi', 'left');
        $this->db_kapi->where('mst_inka_mina.aktif', 'Ya');
        $run_query = $this->db_kapi->get();                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db_kapi->insert('mst_inka_mina', $data);
    }

    public function update($data)
    {
        $this->db_kapi->where('id_kapal', $data['id_kapal']);
        $query = $this->db_kapi->update('mst_inka_mina',$data);
        if($this->db_kapi->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detil_kapal($id)
    {
         $this->db_kapi->select('id_kapal,
                                mst_propinsi.id_propinsi as provinsi,
                                kab_kota,
                                tahun_pembuatan,
                                nama_kapal,
                                tanda_selar,
                                kub_penerima,
                                ketua,
                                anggota,
                                kontak,
                                alamat,
                                sumber_anggaran,
                                bahan_kapal,
                                gt,
                                panjang_kapal,
                                lebar_kapal,
                                dalam_kapal,
                                mesin,
                                daya,
                                pelabuhan_pangkalan,
                                jenis_alat_tangkap,
                                gross_akte,
                                siup,
                                sipi,
                                kontraktor_pembangunan,
                                lokasi_pembangunan');
        $this->db_kapi->from('mst_inka_mina');
        $this->db_kapi->join('mst_kabupaten_kota', 'id_kabupaten_kota = kab_kota', 'left');
        $this->db_kapi->join('mst_propinsi', 'mst_propinsi.id_propinsi = mst_kabupaten_kota.id_propinsi', 'left');
        $this->db_kapi->where('id_kapal', $id);
        
        $run_query = $this->db_kapi->get();                          

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {

        $sql = " UPDATE mst_inka_mina SET aktif='Tidak' WHERE id_kapal=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

	public function from_dss()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}