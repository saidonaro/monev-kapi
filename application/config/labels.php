<?php
	$item = '$item->';
	$config = Array(
					'form_kapal' => Array(
									'title' => Array( 
													'page' => 'Entry Kapal',
													'panel' => 'Entry Kapal'
													),
									'form' => Array(
													'nama_kapal' 				=> Array( 	'name' 	=> 'nama_kapal',
																					 		'label'	=> 'Nama Kapal'
																						),
													'provinsi' 					=> Array( 	'name' 	=> 'provinsi',
																 					  		'label' => 'Provinsi'
 																						),
													'kab_kota' 					=> Array( 	'name' 	=> 'kab_kota',
																 					  		'label' => 'Kabupaten / Kota'
 																						),
													'tahun_pembuatan' 		=> Array( 	'name' 	=> 'tahun_pembuatan',
																					 		'label'	=> 'Tempat Pembuatan'
																						),
													'tanda_selar' 				=> Array( 	'name' 	=> 'tanda_selar',
																					 		'label'	=> 'Tanda Selar'
																						),
													'tahun_pembangunan' 		=> Array( 	'name' 	=> 'tahun_pembangunan',
																					 		'label'	=> 'Tahun Pembangunan'
																						),
													'kub_penerima' 				=> Array( 	'name' 	=> 'kub_penerima',
																					 		'label'	=> 'KUB Penerima'
																						),
													'ketua' 					=> Array( 	'name' 	=> 'ketua',
																					 		'label'	=> 'Ketua'
																						),
													'anggota' 					=> Array( 	'name' 	=> 'anggota',
																					 		'label'	=> 'Anggota'
																						),
													'kontak' 					=> Array( 	'name' 	=> 'kontak',
																					 		'label'	=> 'Kontak'
																						),
													'alamat' 					=> Array( 	'name' 	=> 'alamat',
																					 		'label'	=> 'alamat'
																						),
													'sumber_anggaran' 			=> Array( 	'name' 	=> 'sumber_anggaran',
																					 		'label'	=> 'Sumber Anggaran'
																						),
													'bahan_kapal' 				=> Array( 	'name' 	=> 'bahan_kapal',
																					 		'label'	=> 'Bahan Kapal'
																						),
													'gt' 						=> Array( 	'name' 	=> 'gt',
																					 		'label'	=> 'GT'
																						),
													'panjang_kapal' 			=> Array( 	'name' 	=> 'panjang_kapal',
																					 		'label'	=> 'Panjang Kapal'
																						),
													'lebar_kapal' 				=> Array( 	'name' 	=> 'lebar_kapal',
																					 		'label'	=> 'Lebar Kapal'
																						),
													'dalam_kapal' 				=> Array( 	'name' 	=> 'dalam_kapal',
																					 		'label'	=> 'Dalam Kapal'
																						),
													'mesin' 					=> Array( 	'name' 	=> 'mesin',
																					 			'label'	=> 'Mesin Utama Kapal'
																							),
													'daya' 						=> Array( 	'name' 	=> 'daya',
																					 		'label'	=> 'Daya Mesin Utama Kapal'
																						),
													'pelabuhan_pangkalan' 		=> Array( 	'name' 	=> 'pelabuhan_pangkalan',
																					 		'label'	=> 'Pelabuhan Pangkalan'
																						),
													'jenis_alat_tangkap' 		=> Array( 	'name' 	=> 'jenis_alat_tangkap',
																					 		'label'	=> 'Jenis Alat Tangkap'
																						),
													'gross_akte' 				=> Array( 	'name' 	=> 'gross_akte',
																					 		'label'	=> 'Gross Akte'
																						),
													'no_siup' 					=> Array( 	'name' 	=> 'siup',
																					 		'label'	=> 'No. SIUP'
																						),
													'no_sipi' 					=> Array( 	'name' 	=> 'sipi',
																					 		'label'	=> 'No. SIPI'
																						),
													'kontraktor_pembangunan' 	=> Array( 	'name' 	=> 'kontraktor_pembangunan',
																					 		'label'	=> 'Kontraktor Pembangunan'
																						),
													'lokasi_pembangunan' 		=> Array( 	'name' 	=> 'lokasi_pembangunan',
																					 		'label'	=> 'Lokasi Pembangunan'
																						),
													'foto_pembuatan' 	=> Array( 	'name' 	=> 'foto_pembuatan',
																					 		'label'	=> 'Foto Proses Pembuatan'
																						),
													'foto_sea_trial' 			=> Array( 	'name' 	=> 'foto_sea_trial',
																					 		'label'	=> 'Foto Sea Trial'
																						),
													'foto_oprasional' 			=> Array( 	'name' 	=> 'foto_oprasional',
																					 		'label'	=> 'Foto Oprasional'
																						),
													'foto_siup1' 			=> Array( 	'name' 	=> 'foto_siup1',
																					 		'label'	=> 'Foto Surat Ijin Usaha Pelabuhan l'
																						),
													'foto_siup2' 			=> Array( 	'name' 	=> 'foto_siup2',
																					 		'label'	=> 'Foto Surat Ijin Usaha Pelabuhan 2'
																						),
													'foto_bast1' 			=> Array( 	'name' 	=> 'foto_bast1',
																					 		'label'	=> 'Foto BAST ke KUB 1'
																						),
													'foto_bast2' 			=> Array( 	'name' 	=> 'foto_bast2',
																					 		'label'	=> 'Foto BAST ke KUB 2'
																						),
													),
					
									),
					'view_kapal_table' => Array(
											'title' => Array(
															'page' => 'Data Master Inka Mina 2010 - 2012',
															'panel' => 'Data Master Inka Mina 2010 - 2012'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No'),
															'1' => 'Provinsi',
															'2' => Array('data' => 'Kabupaten/Kota'),
															'3' => 'Tahun Pembuatan',
															'4' => Array('data' => 'Nama Kapal'),
															'5' => Array('data' => 'Tanda Selar'),
															'6' => Array('data' => 'KUB Penerima'),
															'7' => Array('data' => 'Ketua'),
															'8' => 'Anggota',
															'9' => 'Kontak',
															'10' => Array('data' => 'Alamat'),
															'11' => Array('data' => 'Sumber Anggaran'),
															'12' => Array('data' => 'Bahan Kapal'),
															'13' => Array('data' => 'GT'),
															'14' => Array('data' => 'Ukuran Kapal'),
															'15' => Array('data' => 'Mesin'),
															'16' => Array('data' => 'Daya (PK/HP)'),
															'17' => Array('data' => 'Pelabuhan Pangkalan'),
															'18' => Array('data' => 'Jenis Alat Tangkap'),
															'19' => Array('data' => 'Gross Akte'),
															'20' => Array('data' => 'SIUP'),
															'21' => Array('data' => 'SIPI'),
															'22' => Array('data' => 'Kontraktor Pembangunan'),
															'23' => Array('data' => 'Lokasi Pembangunan'),
															'24' => Array('data' => 'Aksi')
														)
											),
					'form_produksi' => Array(
									'title' => Array( 
													'page' => 'Entry Produksi',
													'panel' => 'Entry Produksi'
													),
									'form' => Array(
													'id_kapal'					=> Array(	'name' => 'id_kapal',
																							'label' => 'Id Kapal'
																						),
													'nama_kapal' 				=> Array( 	'name' 	=> 'nama_kapal',
																					 		'label'	=> 'Nama Kapal'
																						),
													'id_alat_tangkap'		=> Array(	'name' => 'id_alat_tangkap',
																							'label' => 'Id Alat Tangkap'
																						),
													'jenis_alat_tangkap' 		=> Array( 	'name' 	=> 'jenis_alat_tangkap',
																					 		'label'	=> 'Jenis Alat Tangkap'
																						),
													'id_wpp'					=> Array(	'name' => 'id_wpp',
																							'label' => 'Id WPP'
																						),
													'wpp' 						=> Array( 	'name' 	=> 'wpp',
																					 		'label'	=> 'WPP'
																						),
													'id_dpi'					=> Array(	'name' => 'id_dpi',
																							'label' => 'Id DPI'
																						),
													'nama_dpi' 					=> Array( 	'name' 	=> 'nama_dpi',
																 					  		'label' => 'Daerah Penangkapan'
 																						),
													'gt' 						=> Array( 	'name' 	=> 'gt',
																					 		'label'	=> 'GT'
																						),


													'tgl_berangkat' 		=> Array( 	'name' 	=> 'tgl_berangkat',
																 					  		'label' => 'Tanggal Berangkat'
 																						),
													'jml_hari_operasi' 		=> Array( 	'name' 	=> 'jml_hari_operasi',
																					 		'label'	=> 'Jumlah Hari Operasi'
																						),
													'jml_ikan' 				=> Array( 	'name' 	=> 'jml_ikan',
																					 		'label'	=> 'Volume (Kg)'
																						),
													'nilai_pendapatan' 			=> Array( 	'name' 	=> 'nilai_pendapatan',
																					 		'label'	=> 'Nilai (Rp)'
																						),
													'id_jenis_ikan'				=> Array(	'name' => 'id_jenis_ikan',
																							'label' => 'Id Jenis Ikan'
																						),
													'jenis_ikan' 				=> Array( 	'name' 	=> 'jenis_ikan',
																					 		'label'	=> 'Jenis Ikan Hasil Tangkapan Dominan'
																						),
													'kebutuhan_bbm' 			=> Array( 	'name' 	=> 'kebutuhan_bbm',
																					 		'label'	=> 'Kebutuhan BBM / Trip (Kl)'
																						),
													'biaya_operasional' 		=> Array( 	'name' 	=> 'biaya_operasional',
																					 		'label'	=> 'Biaya Operasional / Trip (Rp)'
																						),
													'jumlah_abk' 				=> Array( 	'name' 	=> 'jumlah_abk',
																					 		'label'	=> 'Jumlah ABK (orang)'
																						),
													'pendapatan_bersih' 		=> Array( 	'name' 	=> 'pendapatan_bersih',
																					 		'label'	=> 'Pendapatan Bersih / Trip (Rp)'
																						),
													'dana_simpanan_kub' 		=> Array( 	'name' 	=> 'dana_simpanan_kub',
																					 		'label'	=> 'Dana Simpanan KUB (Rp)'
																						),
													'pendapatan_abk' 			=> Array( 	'name' 	=> 'pendapatan_abk',
																					 		'label'	=> 'Pendapatan ABK / Trip (Rp)'
																						),
													'produktivitas_kapal' 		=> Array( 	'name' 	=> 'produktivitas_kapal',
																					 		'label'	=> 'Produktivitas Kapal (Ton/GT Kapal)'
																						),
													'keterangan' 				=> Array( 	'name' 	=> 'keterangan',
																					 		'label'	=> 'Keterangan'
																						)
													)
					
									),
					'view_produksi_table' => Array(
											'title' => Array(
															'page' => 'Data Master Inka Mina 2010 - 2012',
															'panel' => 'Data Master Inka Mina 2010 - 2012'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Kapal', 'width' => '100'),
															'2' =>  Array('data' => 'Jenis Alat Tangkap', 'width' => '100'),//Array('data' => 'Kabupaten/Kota', 'width' => '100'),
															'3' => Array('data' => 'WPP', 'width' => '100'),//'Tahun Pembuatan',
															'4' => Array('data' => 'Daerah Penangkapan', 'width' => '100'),
															'5' =>  Array('data' => 'GT', 'width' => '100'),//Array('data' => 'Tanda Selar', 'width' => '200'),
															'6' => 'Tanggal Berangkat',
															'7' => Array('data' => 'Jumlah Hari Operasi', 'width' => '100'),
															'8' => 'Volume',
															'9' => 'Nilai',
															'10' => Array('data' => 'Jenis Ikan', 'width' => '100'),
															'11' => 'Kebutuhan BBM',
															'12' => 'Biaya Operasional',
															'13' => 'Jumlah ABK',
															'14' => 'Pendapatan Bersih',
															'15' => Array('data' => 'Dana Simpanan KUB', 'width' => '100'),
															'16' => Array('data' => 'Pendapatan ABK', 'width' => '100'),
															'17' => Array('data' => 'Produktivitas Kapal', 'width' => '100'),
															'18' => Array('data' => 'Keterangan', 'width' => '150'),
															'19' => Array('data' => 'Aksi', 'width' => '150')
														)
											),															
			);
/* PETUNJUK PEMAKAIAN :

---Masih di controller---
Load dulu file config labels.php:
$this->load->config('labels');

Simpan ke data constant:
$data['constant'] = $this->config->item('form_pendok');
---Selesai di controller---

---Penggunaan Di file views---
// Menampilkan text untuk page title (biasanya di gunakan di templates header untuk judul page)
echo $constant['title']['page']; 

// Menampilkan text untuk judul panel
echo $constant['title']['panel'];

// Saat set attribut untuk form input, misal field Nomor Surat Permohonan
<label for="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"> 
<?php echo $constant['form']['no_surat_permohonan']['label'];?>
</label>
<input 
type="text" 
id="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
name="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
value=""/>

---Selesai di file views---
*/



/* TEMPLATE:
	// PERHATIKAN TANDA KOMA SEBELUM DAN SESUDAH ARRAY
					'form_kapal' => Array(
									'title' => Array( 
													'page' => 'Halaman Entry Kapal',
													'panel' => 'Entry Kapal'
													),
									'form' => Array(
													'nama_kapal' => Array( 'name' => 'nama_kapal',
																					'label'	=> 'Nama Kapal'
																				),
													'' => Array( 'name' => '',
																 'label' => ''
 																)
													)
									)
*/
?>