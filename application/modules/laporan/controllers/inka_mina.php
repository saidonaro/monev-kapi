<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inka_mina extends MX_Controller {
	
	function __construct()
	{
			parent::__construct();
			$this->load->config('globals');
			$this->load->model('mdl_inka_mina');
			$this->load->model('mdl_propinsi');
	}

	public function index()
	{
		$array_input = $this->input->post(NULL, TRUE);
		$propinsi = isset($array_input['provinsi'])?  $array_input['provinsi'] : 0;
		
		if(isset($array_input['export'])){
			$this->export_inka_mina($propinsi);
		}

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['submit_form'] = 'laporan/inka_mina';
		$data['id_propinsi'] = $propinsi;
		$data['list_kapal'] = $this->mdl_inka_mina->list_inka_mina($propinsi);
		$template = 'templates/page/v_form';
		$modules = 'laporan';
		$views = 'laporan_inka_mina';
		$labels = '';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}
	
	public function info_kapal(){
		$array_input = $this->input->post(NULL, TRUE);
		$kapal = isset($array_input['kapal'])?  $array_input['kapal'] : 0;
		

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['submit_form'] = 'laporan/inka_mina/info_kapal';
		$data['id_kapal'] = $kapal;
		$data['detail_kapal'] = $this->mdl_inka_mina->detail_kapal($kapal);
		// if(isset($array_input['export']) && $data['list_produksi'] != FALSE){
		// 	$this->export_produksi($kapal);
		// }
		$template = 'templates/page/v_form';
		$modules = 'laporan';
		$views = 'laporan_info_kapal';
		$labels = '';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function export_inka_mina($propinsi)
	{
		$id = $propinsi;
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Inka Mina');


		//set tamplate
		//set width
		$this->excel->getActiveSheet()->getColumnDimension("A")->setWidth(7);
		$this->excel->getActiveSheet()->getColumnDimension("B")->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(23);
		$this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(17);
		$this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(23);
		$this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(17);
		$this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(41);
		$this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(24);
		$this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(13);
		$this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(18);
		$this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(61);
		$this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(18);
		$this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(9);
		$this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(8);
		$this->excel->getActiveSheet()->getColumnDimension("O")->setWidth(8);
		$this->excel->getActiveSheet()->getColumnDimension("P")->setWidth(8);
		$this->excel->getActiveSheet()->getColumnDimension("Q")->setWidth(8);
		$this->excel->getActiveSheet()->getColumnDimension("R")->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension("S")->setWidth(12);
		$this->excel->getActiveSheet()->getColumnDimension("T")->setWidth(32);
		$this->excel->getActiveSheet()->getColumnDimension("U")->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension("V")->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension("W")->setWidth(22);
		$this->excel->getActiveSheet()->getColumnDimension("X")->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension("Y")->setWidth(17);
		$this->excel->getActiveSheet()->getColumnDimension("Z")->setWidth(16);
		$this->excel->getActiveSheet()->getColumnDimension("AA")->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension("AB")->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension("AC")->setWidth(22);
		$this->excel->getActiveSheet()->getColumnDimension("AD")->setWidth(23);
		$this->excel->getActiveSheet()->getColumnDimension("AE")->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension("AF")->setWidth(42);
		$this->excel->getActiveSheet()->getColumnDimension("AG")->setWidth(22);

		//border font dan warna
		$styleHeader = array(	'font' => array( 	"bold" => true,
													'color' => array('rgb' => 'FFFFFF')
											),
								'fill' => array(
            						'type' => PHPExcel_Style_Fill::FILL_SOLID,
            						'color' => array('rgb' => '0070C0')
            								)
							);
		$this->excel->getActiveSheet()->getStyle('A3:AG3')->applyFromArray($styleHeader);

		

		/* hedader */
		//set nama header table
		$this->excel->getActiveSheet()	->setCellValue('A1' , 'DATA MASTER INKA MINA');
		$this->excel->getActiveSheet()->getStyle('A1')->applyFromArray(array(	'font' => array( 	"bold" => true)));


		$this->excel->getActiveSheet()	->setCellValue('A3' , 'NO.')
										->setCellValue('B3' , 'Provinsi')
										->setCellValue('C3' , 'Kab/Kota')
										->setCellValue('D3' , 'Tahun Pembuatan (10/11/12)')
										->setCellValue('E3' , 'Nama Kapal')
										->setCellValue('F3' , 'Tanda Selar')
										->setCellValue('G3' , 'KUB Penerima')
										->setCellValue('H3' , 'Ketua')
										->setCellValue('I3' , 'Anggota (orang)')
										->setCellValue('J3' , 'Kontak KUB')
										->setCellValue('K3' , 'Alamat')
										->setCellValue('L3' , 'Sumber Anggaran (TP/DK)')
										->setCellValue('M3' , 'Bahan Kapal')
										->setCellValue('N3' , 'GT')
										->setCellValue('O3' , 'P (m)')
										->setCellValue('P3' , 'L (m)')
										->setCellValue('Q3' , 'D (m)')
										->setCellValue('R3' , 'Mesin')
										->setCellValue('S3' , 'Daya (PK/HP)')
										->setCellValue('T3' , 'Pelabuhan Pangkalan')
										->setCellValue('U3' , 'Jenis Alat Tangkap')
										->setCellValue('V3' , 'Grosse Akte/Buku Kapal')
										->setCellValue('W3' , 'SUP')
										->setCellValue('X3' , 'SIPI')
										->setCellValue('Y3' , 'Total Produksi (kg)')
										->setCellValue('Z3' , 'Jumlah Trip Selama ini (kali)')
										->setCellValue('AA3' , 'Rata-rata Jumlah hari/trip (Hari)')
										->setCellValue('AB3' , 'Rata-rata Produksi (Kg/Kapal/Trip)')
										->setCellValue('AC3' , 'Rata-rata Kebutuhan BBM/Trip (Tonk/Kapal/Trip)')
										->setCellValue('AD3' , 'Rata-rata Operasional/Trip (RP/Kapal/Trip)')
										->setCellValue('AE3' , 'Rata-rata Pendapatan Bersih (Rp/Kapal/Trip)')
										->setCellValue('AF3' , 'Kontraktor Pelaksanaan Pembangunan')
										->setCellValue('AG3' , 'Lokasi Pembangunan Kapal');

		//ambil data tabel
		$data['list_kapal'] = $this->mdl_inka_mina->list_inka_mina($id);

		$jumlah_data;
		if(count($data) != 0){
			$index = 1;//no data
			foreach ($data['list_kapal'] as $item) {
				$this->excel->getActiveSheet()	->setCellValue('A'.($index+3) , $index)
												->setCellValue('B'.($index+3) , $item->nama_propinsi)
												->setCellValue('C'.($index+3) , $item->nama_kabupaten_kota)
												->setCellValue('D'.($index+3) , $item->tahun_pembuatan)
												->setCellValue('E'.($index+3) , $item->nama_kapal)
												->setCellValue('F'.($index+3) , $item->tanda_selar)
												->setCellValue('G'.($index+3) , $item->kub_penerima)
												->setCellValue('H'.($index+3) , $item->ketua)
												->setCellValue('I'.($index+3) , $item->anggota!=0 ? $item->anggota : '')
												->setCellValue('J'.($index+3) , $item->kontak)
												->setCellValue('K'.($index+3) , $item->alamat)
												->setCellValue('L'.($index+3) , $item->sumber_anggaran)
												->setCellValue('M'.($index+3) , $item->bahan_kapal)
												->setCellValue('N'.($index+3) , $item->gt)
												->setCellValue('O'.($index+3) , $item->panjang_kapal!=0 ? $item->panjang_kapal : '')
												->setCellValue('P'.($index+3) , $item->lebar_kapal!=0? $item->lebar_kapal : '')
												->setCellValue('Q'.($index+3) , $item->dalam_kapal!=0? $item->dalam_kapal : '')
												->setCellValue('R'.($index+3) , $item->mesin)
												->setCellValue('S'.($index+3) , $item->daya!=0? $item->daya : '')
												->setCellValue('T'.($index+3) , $item->pelabuhan_pangkalan)
												->setCellValue('U'.($index+3) , $item->jenis_alat_tangkap)
												->setCellValue('V'.($index+3) , $item->gross_akte)
												->setCellValue('W'.($index+3) , $item->siup)
												->setCellValue('X'.($index+3) , $item->sipi)
												->setCellValue('Y'.($index+3) , $item->jml_produksi)
												->setCellValue('Z'.($index+3) , $item->jml_opreasi)
												->setCellValue('AA'.($index+3) , $item->avg_hari_operasi!==0?(int) $item->avg_hari_operasi:'')
												->setCellValue('AB'.($index+3) , $item->avg_produksi)
												->setCellValue('AC'.($index+3) , $item->avg_bbm)
												->setCellValue('AD'.($index+3) , $item->avg_operasional)
												->setCellValue('AE'.($index+3) , $item->avg_pendapatan)
												->setCellValue('AF'.($index+3) , $item->kontraktor_pembangunan)
												->setCellValue('AG'.($index+3) , $item->lokasi_pembangunan);
				$index++;
			}
			$jumlah_data = $index;
		}

		//border table
		$styleArray = array(
		  	'borders' => array(
		    	'allborders' => array(
		      		'style' => PHPExcel_Style_Border::BORDER_THIN
		    	)
		  	)
		);

		$this->excel->getActiveSheet()->getStyle('A3:AG'.($jumlah_data+3))->applyFromArray($styleArray);
		//center secara horizontal
		$this->excel->getActiveSheet()->getStyle('A3:AG3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		//center secara vertical
		$this->excel->getActiveSheet()->getStyle('A3:AG'.($jumlah_data+3))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		//texwarp true
		$this->excel->getActiveSheet()->getStyle('A3:AG'.($jumlah_data+3))->getAlignment()->setWrapText(true);
		// die;

		$filename='Data Master Inka Mina.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		// //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		// //if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		// //force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
		
	}

	

}