<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_wpp extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_wpp_json()
	{
		$this->load->model('mdl_wpp');

		$list_opsi = $this->mdl_wpp->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_wpp_array()
	{
		$this->load->model('mdl_wpp');

		$list_opsi = $this->mdl_wpp->list_opsi();

		return $list_opsi;
	}
	
}
?>