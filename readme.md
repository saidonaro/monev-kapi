SETUP AWAL
=========

Aplikasi MONEV KAPI.

### SETUP APLIKASI CODEIGNITER:
  - Clone via source tree ke folder monev_kapi di htdocs
  - Buat folder uploads/document di root folder monev_kapi
  - buat folder assets/monevkapi/cs, assets/monevkapi/images/, assets/monevkapi/js
  - buat folder assets/third_party/
  - Extract semua zip file di folder misc/bahan_awal/
  - file assets_awal.zip mengandung tiga folder (css, js, images), Copy/paste tiga folder ini ke dalam folder assets/third_party/
  - extract file zip system.zip dan paste di root folder monev_kapi
  - extract file zip config_awal.zip (config.php, database.php), Copy/Paste ke folder application/config/

### SETUP DATABASE MYSQL:
  - Masuk ke phpmyadmin
  - Buat database monev_kapi

### SETUP MPDF:
  - Download MPDF : [MPDF57.zip](http://mpdf1.com/repos/MPDF57.zip)
  - Extract MPDF57.zip, rename jadi folder mpdf. MOVE ke folder application/libraries/
