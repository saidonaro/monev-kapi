<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produksi extends MX_Controller {
	
	function __construct()
	{
			parent::__construct();
			$this->load->model('mdl_produksi');
	}

	public function index()
	{
		$array_input = $this->input->post(NULL, TRUE);
		$kapal = isset($array_input['kapal'])?  $array_input['kapal'] : 0;
		

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['submit_form'] = 'laporan/produksi';
		$data['id_kapal'] = $kapal;
		$data['list_produksi'] = $this->mdl_produksi->detail_list_produksi($kapal);
		if(isset($array_input['export']) && $data['list_produksi'] != FALSE){
			$this->export_produksi($kapal);
		}
		$template = 'templates/page/v_form';
		$modules = 'laporan';
		$views = 'laporan_produksi';
		$labels = '';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function export_produksi($id_kapal)
	{
		$id = $id_kapal;
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		$data['list_produksi'] = $this->mdl_produksi->detail_list_produksi($id);


		//set tamplate
		//set width
		$this->excel->getActiveSheet()->getColumnDimension("A")->setWidth(5);
		$this->excel->getActiveSheet()->getColumnDimension("B")->setWidth(10.57);
		$this->excel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(9.29);
		$this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(11.29);
		$this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(11.43);
		$this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(11.14);
		$this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(11.29);
		$this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(20.59);
		$this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(10.43);
		$this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(13.14);
		$this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(8.43);
		$this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(22.29);
		$this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(14.14);
		$this->excel->getActiveSheet()->getColumnDimension("O")->setWidth(18.43);
		$this->excel->getActiveSheet()->getColumnDimension("P")->setWidth(18.3);
		$this->excel->getActiveSheet()->getColumnDimension("Q")->setWidth(18.86);
		
		$this->excel->getActiveSheet()->getRowDimension("2")->setRowHeight(8);

		/* hedader */
		//set marge
		$this->excel->getActiveSheet()	->mergeCells("A1:E1")
										->mergeCells("A3:C3")
										->mergeCells("A4:C4")
										->mergeCells("A5:A6")
										->mergeCells("B5:B6")
										->mergeCells("C5:C6")
										->mergeCells("D5:D6")
										->mergeCells("E5:E6")
										->mergeCells("F5:F6")
										->mergeCells("J5:J6")
										->mergeCells("K5:K6")
										->mergeCells("L5:L6")
										->mergeCells("M5:M6")
										->mergeCells("N5:N6")
										->mergeCells("O5:O6")
										->mergeCells("P5:P6")
										->mergeCells("Q5:Q6")
										->mergeCells("G5:I5");
		//center secara horizontal
		$this->excel->getActiveSheet()->getStyle('A5:Q7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		//center secara vertical
		$this->excel->getActiveSheet()->getStyle('A5:Q7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		//texwarp true
		$this->excel->getActiveSheet()->getStyle('A5:Q7')->getAlignment()->setWrapText(true); 

		$this->excel->getActiveSheet()->getStyle('A1')->applyFromArray(array('font' => array( "bold" => true)));
		$this->excel->getActiveSheet()->setCellValue('A1' , 'DATA PRODUKSI KAPAL INKA MINA');
		$this->excel->getActiveSheet()->setCellValue('A3' , 'NAMA KAPAL                                   :'.$data['list_produksi'][0]->nama_kapal);
		$this->excel->getActiveSheet()->setCellValue('A4' , 'JENIS ALAT PENANGKAP IKAN  :');
		
		//border font dan warna
		$styleHeader = array(	'font' => array( "bold" => true),
								'fill' => array(
            						'type' => PHPExcel_Style_Fill::FILL_SOLID,
            						'color' => array('rgb' => 'C4D79B'))
							);
		$this->excel->getActiveSheet()->getStyle('A5:Q7')->applyFromArray($styleHeader);

		//set nama header table
		$this->excel->getActiveSheet()->setCellValue('A5' , 'NO.');
		$this->excel->getActiveSheet()->setCellValue('B5' , 'WPP');
		$this->excel->getActiveSheet()->setCellValue('C5' , 'Daerah Penangkalan');
		$this->excel->getActiveSheet()->setCellValue('D5' , 'GT Kapal');
		$this->excel->getActiveSheet()->setCellValue('E5' , 'Tgl Berangkat');
		$this->excel->getActiveSheet()->setCellValue('F5' , 'Jumlah Hari Operasi');
		$this->excel->getActiveSheet()->setCellValue('G5' , 'Produksi/Trip');
		$this->excel->getActiveSheet()->setCellValue('G6' , 'Volume (Kg)');
		$this->excel->getActiveSheet()->setCellValue('H6' , 'Nilai (Rp)');
		$this->excel->getActiveSheet()->setCellValue('I6' , 'Jenis Ikan Hasil Tangkapan Dominan');
		$this->excel->getActiveSheet()->setCellValue('J5' , 'Kebutuhan BBM/TRIP (Ton)');
		$this->excel->getActiveSheet()->setCellValue('K5' , 'Biaya Operasional/Trip (RP)');
		$this->excel->getActiveSheet()->setCellValue('L5' , 'Jumlah ABK (orang)');
		$this->excel->getActiveSheet()->setCellValue('M5' , 'Pendapatan Bersih/Trip (Rp)');
		$this->excel->getActiveSheet()->setCellValue('N5' , 'Dana Simapana KUB');
		$this->excel->getActiveSheet()->setCellValue('O5' , 'Pendapatan ABK/Trip (Rp)');
		$this->excel->getActiveSheet()->setCellValue('P5' , 'Produktivitas Kapal (Ton/GT Kapan)');
		$this->excel->getActiveSheet()->setCellValue('Q5' , 'Keterangan');

		$this->excel->getActiveSheet()	->setCellValue('A7' , 1)
										->setCellValue('B7' , 2)
										->setCellValue('C7' , 3)
										->setCellValue('D7' , 4)
										->setCellValue('E7' , 5)
										->setCellValue('F7' , 6)
										->setCellValue('G7' , 7)
										->setCellValue('H7' , 8)
										->setCellValue('I7' , 9)
										->setCellValue('J7' , 10)
										->setCellValue('K7' , 11)
										->setCellValue('L7' , 12)
										->setCellValue('M7' , '13=8-11')
										->setCellValue('N7' , 14)
										->setCellValue('O7' , '15=(13-14)/12')
										->setCellValue('P7' , '16=(7/1000)/4')
										->setCellValue('Q7' , 17);

		
		$jumlah_data;
		if(count($data) != 0){
			$index = 1;//no data
			foreach ($data['list_produksi'] as $item) {
				$jumlah_data = $index;
				$this->excel->getActiveSheet()->setCellValue('A'.($index+7) , $index);//nomor
				$this->excel->getActiveSheet()->setCellValue('B'.($index+7) , $item->id_wpp);
				$this->excel->getActiveSheet()->setCellValue('C'.($index+7) , $item->nama_wpp);
				$this->excel->getActiveSheet()->setCellValue('D'.($index+7) , $item->gt);
				$this->excel->getActiveSheet()->setCellValue('E'.($index+7) , $item->tgl_berangkat);
				$this->excel->getActiveSheet()->setCellValue('F'.($index+7) , $item->jml_hari_operasi);
				$this->excel->getActiveSheet()->setCellValue('G'.($index+7) , $item->jml_ikan);
				$this->excel->getActiveSheet()->setCellValue('H'.($index+7) , $item->nilai_pendapatan);
				$this->excel->getActiveSheet()->setCellValue('I'.($index+7) , $item->nama_jenis_ikan);
				$this->excel->getActiveSheet()->setCellValue('J'.($index+7) , $item->kebutuhan_bbm);
				$this->excel->getActiveSheet()->setCellValue('K'.($index+7) , $item->biaya_operasional);
				$this->excel->getActiveSheet()->setCellValue('L'.($index+7) , $item->jumlah_abk);
				$this->excel->getActiveSheet()->setCellValue('M'.($index+7) , $item->pendapatan_bersih);
				$this->excel->getActiveSheet()->setCellValue('N'.($index+7) , $item->dana_simpanan_kub);
				$this->excel->getActiveSheet()->setCellValue('O'.($index+7) , $item->pendapatan_abk);
				$this->excel->getActiveSheet()->setCellValue('P'.($index+7) , $item->produktivitas_kapal);
				$this->excel->getActiveSheet()->setCellValue('Q'.($index+7) , $item->keterangan);
				$index++;
			}
		}

		//borde
		$styleArray = array(
		  	'borders' => array(
		    	'allborders' => array(
		      		'style' => PHPExcel_Style_Border::BORDER_THIN
		    	)
		  	)
		);

		$this->excel->getActiveSheet()->getStyle('A5:Q'.($jumlah_data+7))->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getStyle('B2:B'.($jumlah_data+7))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$filename='Form Produksi.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');

	}

	

}